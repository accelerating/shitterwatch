function save_options() {
    var apikey = document.getElementById('api').value;

    chrome.storage.local.set({
        shitterwatch: {
            apikey: apikey
        }
    }, function () {
        var status = document.getElementById('status');
        status.textContent = 'Options saved.';
        setTimeout(function () {
            status.textContent = '';
        }, 750);
    });

}

function restore_options() {
    chrome.storage.local.get('shitterwatch', function (res) {
        document.getElementById('api').value = res.shitterwatch.apikey ? res.shitterwatch.apikey : '';
    });
}
document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click', save_options);