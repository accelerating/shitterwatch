//User class.
//A user is a poster on the forum. A instance is created for each unique poster on a page.
class User
{
	constructor(name, world)
	{
		this.name = name;
		this.world = world;
		this.location = getLocation(world);
		
		//Reference to the HTML object to render our information.
		this.HTML_Elements = [];
		//Array of encounters. This information is pulled from the FF Logs API by the fflogsAPI function (See fflogsAPI.js)
		this.encounters = [];
		//Map of jobs.  Gets populated in the updateJobs() function after the logs are successfully fetched. 
		//Key are FFXIV jobs as written in the FF Logs API.
		this.jobs = new Map();
		//Boolean to check if the logs could be fetched from the FF Logs API.
		this.fetchedSuccessfully;
		//If there's an error (usually too many requests) we store its message here and display it instead of the usual information.
		this.errorMessage;
		//Characters logs on FF Logs can be 'hidden'. In this case we display the 'Thonking.png' cuz that's hilarious. 
		this.hidden = false;
	}

	//Adds an element to the HTLM_Element array.
	addHTML_Element(HTML_Element)
	{
		this.HTML_Elements.push(HTML_Element);
	}

	//Gets the logs for this user. This function calls the fflogsAPIRequest method and handles the further processings depending on the response.
	getLogs()
	{
		let user = this,
			request = fflogsAPIRequest(user);
		
        //The request variable is a promise. 
		request.then(response =>
		{
			//Request without error
			//Response is either an array of encounters (possibly empty) or the object {hidden : true }
			user.fetchedSuccessfully = true;
			//Check if response is hidden
			if (response && response.hidden === true)
			{
				//Update the user's hidden Boolean
				user.hidden = true;
			}
			else
			{
				//If the response isn't hidden we add all encounters into the user.encounters array.
				user.encounters.push(...response);
				//Query the encounters and place them into the user.job Map. Each job has its own key and the encounters get added into an array in the object with the correct key.
				user.sortEncountersByJobs();
				//The data inside each Map object now gets processed. We now calculate the stats for each job.
				user.updateJobs();
			}

		}).catch(response =>
		{
			//An error happend. This usually means either the character does not exist on FF Logs or we passed the rate limited and the given API Key gets temporarily restriced from accessing FF Logs.
			user.fetchedSuccessfully = false;
			//Update the error message.
			user.errorMessage = response;
		}).finally(response =>
		{
			//All said an done. The request has either succeeded or failed, now we render the results under each post.
			user.render();
		});
	}

	//We have all the encounters in the user.encounters array. We now place each in the the user.jobs Map with relevant job as key. 
	//E.g.	(Key, 			Value											)
	//		('Black Mage', 	{encounters[...], count, average, median, best}	)
	sortEncountersByJobs()
	{
		var user = this;
		//Query each encounter in the user.encounters array

		for (let encounter of user.encounters)
		{
			//If no map with the current job as key (e.g. 'Black Mage') exists then create a new one. We also already create some fields for the percentiles later.
			if (!user.jobs.has(encounter.spec))
			{
				user.jobs.set(encounter.spec,
				{
					encounters: [],
					count: undefined,
					average: undefined,
					median: undefined,
					best: undefined
				});
			}

			//Add the encounter into the Map's object with the correct job as key (e.g. 'Black Mage')
			user.jobs.get(encounter.spec).encounters.push(encounter);
		}
	}

	//We have a bunch of Objects (or possibly none if there are no logs) in the user.jobs Map. Each object represents a job (e.g. 'Black Mage') and holds all encounters for that job. We now proccess the FF Logs percentiles informatin and calculate the average, median and best percentiles from this.
	updateJobs()
	{
		var user = this;

		//Query each job 
		user.jobs.forEach((job, key) =>
		{
			//Each encounter has a 'percentile' infomation. It's basically represents your e-penis and goes from 0 (really uncool) to 100 (cash af). We add each percentile and add it into an array and also sort it from lowest to highest.
			let percentiles = job.encounters.reduce((acc, encounter) =>
			{
				//Add each encounter into a temporarily array.
				acc.push(encounter.percentile);
				return acc;
				//Also sort the array.
			}, []).sort((a, b) => a - b);
			//percentiles now looks like this: [10, 30, 60, 99]
			//We now use this array to calculate our stats.
			
			//Number of encounters for this job
			job.count = percentiles.length;

			//Average percentile 
			job.average = Math.floor(percentiles.reduce((acc, value) => acc + value, 0) / job.count).toFixed(1);

			//Median encounter 
			job.median = percentiles[Math.floor(percentiles.length / 2)].toFixed(1);

			//The best (most padded) percentile
			job.best = Math.max(...percentiles).toFixed(1);
		});
	}

	//Renders the data (percentiles in various formats and job icons OR error log) under the post(s). 
	render()
	{
		var user = this;
		//Base URL to open the character page if we click on the container containing our information.
		const urlClick = 'https://www.fflogs.com/character/';

		for (let HTML_Element of user.HTML_Elements)
		{
			//Remove the loading spinner.
			HTML_Element.classList.remove('shitterwatch-prerender');
			//Check if the data was loaded successfully and if the character's log are hiddden to render the correct stuff.
			if (user.fetchedSuccessfully && !user.hidden)
			{
				//The logs were loaded successfully and the character isn't hidden. We'll show the job icons and the stats underneath. 
				
				//Click element. If we click on the container containing the icons the player's FF Log page will be loaded.
				HTML_Element.addEventListener('click', function ()
				{
					window.open(encodeURI(`${urlClick}${user.location}/${user.world}/${user.name}`), '_blank');

				});

				//For each job, load the image and stats.
				user.jobs.forEach((job, key) =>
				{
					HTML_Element.appendChild(createJobDiv(job, key));

				});
			}

			else if (user.hidden)
			{
				//Yikes
				//The logs are hidden. Load the default hidden text and the Thonking.png.
				HTML_Element.appendChild(createHiddenDiv());
			}
			else
			{
				//The data wasn't loaded successfully. Show the error message.
				HTML_Element.append(createNoDataDiv(user.errorMessage));
			}

		}

	}
}
