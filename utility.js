//Here are some utility functions. The reason they are in a seprate file is mainly for clarity's sake.

//This function returns the Datacenter ('location') of the server. This is either US, NA or JP.
function getLocation(world)
{
	for (let location in DATACENTER)
	{
		//We check which object in the Datacenter variable contains the world (eg. 'Aegis'). The name of that object is the Datacenter.
		if (DATACENTER[location].includes(world))
		{
			return location;
		}
	}
	return null;
}

//Here we create the 'job div'. This is the HTML element which contains the job image and stats. It also has the relevant CSS class.
function createJobDiv(job, key)
{
	//Create a parent container.
	let jobDiv = document.createElement('div');
	jobDiv.classList.add('shitterwatch-jobDiv');

	//Text container. Here's we'll show the texts.
	let textDiv = document.createElement('div');
	textDiv.classList.add('shitterwatch-textDiv');

	//We'll pass the formatText = false flag for the count text, because we want the number following the count text to be black. 
	textDiv.appendChild(divBuilder('runs', job.count, false));
	//The other numbers we'll be formatted depending on the number value.
	textDiv.appendChild(divBuilder('best', job.best));
	textDiv.appendChild(divBuilder('average', job.average));
	textDiv.appendChild(divBuilder('median', job.median));

	//Here we'll place the job icon.
	let imgDiv = document.createElement('img');
	imgDiv.setAttribute('src', chrome.extension.getURL(`icons/${key}.png`));
	imgDiv.classList.add('shitterwatch-imgDiv');

	//Add it to the parent container.
	jobDiv.appendChild(imgDiv);
	jobDiv.appendChild(textDiv);
	return jobDiv;
}

//Basically same as above. This is the HTML element which contains the Thonking.png and a funny text for the lol (TL note: lol means 'Lots Of Laugher') 
function createHiddenDiv()
{
	//NB: The Div isn't actually hidden, it's just its name.
	const hiddenIcon = 'Thonking.png';

	let hiddenDiv = document.createElement('div');
	hiddenDiv.classList.add('shitterwatch-hiddenDiv');

	let imgDiv = document.createElement('img');
	imgDiv.setAttribute('src', chrome.extension.getURL(`icons/${hiddenIcon}`));
	imgDiv.classList.add('shitterwatch-imgDiv');

	let textDiv = document.createElement('div');
	textDiv.textContent = 'Character hidden on fflogs';
	textDiv.classList.add('shitterwatch-noDataDiv');

	hiddenDiv.appendChild(imgDiv);
	hiddenDiv.appendChild(textDiv);

	return hiddenDiv;
}

//Same again. We'll render the div to show the error message.
function createNoDataDiv(error)
{
	let noDataDiv = document.createElement('div');
	noDataDiv.classList.add('shitterwatch-noDataDiv');
	noDataDiv.textContent = error;
	return noDataDiv;
}

//This gets used by the createJobDiv() function. This function builds a div element with mixed formatting: On half is rendered without special formatting, the other half gets formatted depending on the number value. It is used to create the text for the best/average/median/count. It gets called with the formatText=false flag by the text showing the number of runs (eg. 'runs: 100' gets rendered without the '100' being yellow).
function divBuilder(normalText, formattedText, formatText = true)
{
	let div = document.createElement('div');

	//Create a span and place the normalText into it. (i.e. 'best:')
	let spanNormal = document.createElement('span');
	spanNormal.textContent = `${normalText}: `;
	div.appendChild(spanNormal);

	//The text which gets formatted (i.e. '100' - this text usually gets formatted depending on the number value. The number following the number of runs ('count' variable) doesn't get formatted).
	let spanFormatted = document.createElement('span');
	spanFormatted.textContent = `${formattedText}`;
	if (formatText)
	{
		spanFormatted.classList.add(getColorCls(formattedText));
	}
	div.appendChild(spanFormatted);

	return div;
}

//This function returns the CSS class based on the number value. It is used by the div builder to color the percentiles the same was as FF Logs does.
function getColorCls(value)
{
	//The CSS class names are the same as those on FF Logs. Pretty cool eh?
	if (value == 100)
	{
		return 'artifact';
	}

	if (value >= 95)
	{
		return 'legendary';
	}

	if (value >= 75)
	{
		return 'epic';
	}

	if (value >= 50)
	{
		return 'rare';
	}

	if (value >= 20)
	{
		return 'uncommon';
	}
	//No bully tho.
	return 'common';
}
