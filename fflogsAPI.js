//The function to create an xhr request to the FF Logs API. This gets called by the user class in the user.getLogs() function. This function returns a Promise with the xhr request.
function fflogsAPIRequest(user)
{
	let baseURL = 'https://www.fflogs.com/v1/parses/character/',
		//URL used for the xhr request.    
		url = `${baseURL}${user.name}/${user.world}/${user.location}?&api_key=${apikey}`;

	//What is returned to the user class.
	return new Promise((resolve, reject) =>
	{
		//Create a xhr request with the constructed url and open it.
		let xhr = new XMLHttpRequest();
		xhr.open('GET', url);

		xhr.onreadystatechange = function ()
		{
			if (xhr.readyState === 4 && xhr.status === 200)
			{
				//Success! The request was successful.
				//Let's check if the response contains encounters (i.e. there are relevant logs on FF Logs) or if the response is empty (i.e. there are no relevant logs on FF Logs)
				//We either resolve with the data or an empty string.
				xhr.response ? resolve(JSON.parse(xhr.response)) : resolve([]);
			}
			else if (xhr.readyState === 4)
			{
				//Failure! There was an error with the request.
				if (xhr.statusText === 'Bad Request')
				{
					//This error means that the character does not exists on FF Logs. Will show a modified error log to clearly state the issue.
					reject('Character does not exist on FF Logs.');
				}
				else
				{
					//In all other cases we'll log the error to the console and render the error. 
					console.warn(`Request Error: Url: ${url} Message: ${xhr.statusText} Status: ${xhr.status}`);
					reject(xhr.statusText);
				}
			}
		};
		xhr.send();
	});
}
