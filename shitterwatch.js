/*
    Author: 		Accelerating
    Version: 		2 
    Last change: 	21.07.2019
    Website:		https://bitbucket.org/accelerating/shitterwatch/

    License:
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

//Global variable where the entered API key is stored
var apikey;

//Datacenters. Used to find the server's continent.
const DATACENTER = {
	JP: [
		'Aegis',
		'Atomos',
		'Carbuncle',
		'Garuda',
		'Gungnir',
		'Kujata',
		'Ramuh',
		'Tonberry',
		'Typhon',
		'Unicorn',
		'Alexander',
		'Bahamut',
		'Durandal',
		'Fenrir',
		'Ifrit',
		'Ridill',
		'Tiamat',
		'Ultima',
		'Valefor',
		'Yojimbo',
		'Zeromus',
		'Anima',
		'Asura',
		'Belias',
		'Chocobo',
		'Hades',
		'Ixion',
		'Mandragora',
		'Masamune',
		'Pandaemonium',
		'Shinryu',
		'Titan'
	],
	NA: [
		'Adamantoise',
		'Cactuar',
		'Faerie',
		'Gilgamesh',
		'Jenova',
		'Midgardsormr',
		'Sargatanas',
		'Siren',
		'Behemoth',
		'Excalibur',
		'Exodus',
		'Famfrit',
		'Hyperion',
		'Lamia',
		'Leviathan',
		'Ultros ',
		'Balmung',
		'Brynhildr',
		'Coeurl',
		'Diabolos',
		'Goblin',
		'Malboro',
		'Mateus',
		'Zalera'
	],
	EU: [
		'Cerberus',
		'Louisoix',
		'Moogle',
		'Omega',
		'Ragnarok',
		'Lich',
		'Odin',
		'Phoenix',
		'Shiva',
		'Zodiark',
		'Spriggan',
		'Twintania'
	]
};

//Entry point. Calls the initConfig and then asynchronically calls the initUsers method.
function main()
{
	initConfig(() =>
	{
		initUsers();
	});

}

//Loads the CSS and asynchronically retrieves the API key from the local browser store.
function initConfig(callback)
{
	var head = document.head,
		link = document.createElement("link");

	//Load CSS
	link.type = "text/css";
	link.rel = "stylesheet";
	link.href = chrome.extension.getURL('shitterwatch.css');
	head.appendChild(link);

	//Load API Key from store
	chrome.storage.local.get('shitterwatch', function (res)
	{
		var result = res.shitterwatch ||
		{};
		if (result.apikey)
		{
			//Remove any mistyped whitespaces
			apikey = result.apikey.trim('');
		}
		else
		{
			//No key found. Abort mission.
			console.log("No api key entered!");
			return;
		}

		console.log(`Entered api key: ${apikey}`);
		callback();
	});
}

//Query the pages posts, create a user class for each unique poster and call the User.getLogs() method.
function initUsers()
{
	var posts = document.getElementsByClassName('postcontainer'),
		users = [],
		name, world, el, index;

	const DOM = {
		USER: 1,
		WORLD: 3
	};

	//Query each post
	for (let post of posts)
	{
		let userInfo = post.getElementsByClassName('userinfo_mainchar')[0];

		//Check if the post has any user info. Deleted posts or special posts may lack this information.
		if (userInfo)
		{
			//Given character name
			name = userInfo.children[DOM.USER].innerHTML;
			//Given server
			world = userInfo.children[DOM.WORLD].innerHTML;

			//Add some CSS to the 'cleardiv' - a div at the end of each post used to place our stuff.
			let clearDiv = post.getElementsByClassName('cleardiv')[0];
			clearDiv.classList.add('shitterwatch-container');
			//Shows a cute loading spinner owo
			clearDiv.classList.add('shitterwatch-prerender');

			//Check if the current poster is already in the user array. This happens if a posters posts multiple times on one page.
			index = users.findIndex(element => { 
				return element.name === name && element.world === world;
			});

			if (index === -1)
			{
				//New user - create a class and add the current HTML reference.
				let newUser = new User(name, world);
				newUser.addHTML_Element(clearDiv);
				users.push(newUser);
			}
			else
			{
				//User class already created for this poster. Only add the current HTML reference to the class instance.
				users[index].addHTML_Element(clearDiv);
			}
		}
	}

	//We now have all the users on the current site as User class. Let's call the getLogs() method on each of them to get this thing started.
	for (user of users)
	{
		user.getLogs();
	}
}

main();
